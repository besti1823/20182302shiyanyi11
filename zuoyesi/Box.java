public class Box
{
    private double height,width,depth;
    private boolean full;
    public Box(double h, double w, double d) 
    {
        this.height=h;
        this.width=w;
        this.depth=d;
        full=false;
    }
    public double getheight() 
    {
        return height;
    }
    public void setheight(double h) 
    {
        this.height=h;
    }
    public double getwidth() 
    {
        return width;
    }
    public void setwidth(double w) 
    {
        this.width=w;
    }
    public double getdepth()
    {
        return depth;
    }
    public void setdepth(double d) 
    {
        this.depth=d;
    }
    public boolean getfull() 
    {
        return full;
    }
    public void setfull(boolean full)
    {
        this.full=full;
    }
    @Override
    public String toString() 
    {
        return "The wide of the box is :" + width+"/nThe depth of the box is :"+depth+"/nThe height of the box is :"+height;
    }
}
