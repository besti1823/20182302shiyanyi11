public class FlipRace 
{
    public static void main(String[] args) 
     {
        final int target= 3;
        int a=0,b=0;
        coin c=new coin();
        coin d=new coin();
        while (a<target && b<target)
        {
            c.flip();
            d.flip();
            System.out.println("Coin 1: "+c+"  "+"Coin 2: "+d);
            a=(c.isHeads())?a+1:0;
            b=(d.isHeads())?b+1:0;
        }
        if (a<target)
            System.out.println("b Wins!");
        else if (b<target)
            System.out.println("a Wins!");
        else
            System.out.println("Ping");
    }
}
